# 0.4 Release Candidate

* Vastly improved initial setup
* Upgraded Organizr to v2
* Added The Lounge - IRC Bouncer
* Added Radarr - DVR
* Added Sonarr - DVR
* Added Kibitzr - IFTTT replacement
* Added BulletNotes - Note taking application
* Added Emby - Personal Media Server
* Automated Grafana Configuration
* Added Cloud Bastion Server via Tinc VPN option
* Added individual service toggling via host vars
* Removed Koel - Rarely worked
* Removed Convos - Rarely worked

# 0.3

* Added Semi-automated Apple Health Import
* Added Automated HTTPS via LetsEncrypt
* Added Tor Onion Services - Remote access through any firewall
* Added OpenVPN - Remote device Ad filtering via piHole
* Added Mastodon - Federated social micro blogging
* Added Matomo - Web analytics
* Added Bitwarden - Password manager
* Added piHole - Network wide ad blocking

# 0.2

* Added Dasher / Amazon Dash Button support

# 0.1

* Initial release